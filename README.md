README
======

A simple wrapper around GraphViz-like tools for MATLAB. I use script to generate dependency graphics for CellOrganizer.

Dependencies
------------

* [GraphViz-like tools for MATLAB](http://www.mathworks.com/matlabcentral/fileexchange/27608-graphviz-like-tools-for-matlab). mkdotfile creates a GraphViz dot language representation of the dependencies of a function. mGraphViz plots the directed graph onto a figure. These functions are a small part of a larger project to implement a doxygen like code documentation system within MATLAB that doesn't have any external program dependencies.

* [fdep: a pedestrian function dependencies finder](http://www.mathworks.com/matlabcentral/fileexchange/17291-fdep--a-pedestrian-function-dependencies-finder). FDEP dissects MATLAB files and iteratively looks for all user defined functions (modules), which are used during runtime.

* [GraphViz](http://www.graphviz.org/). Graphviz is open source graph visualization software. Graph visualization is a way of representing structural information as diagrams of abstract graphs and networks.

Installation
------------

You need to install GraphViz. I used [HomeBrew](http://brew.sh/) in my personal laptop. The formula is:

	brew install graphviz

This will install ```dot``` in ```/usr/local/bin/```.
